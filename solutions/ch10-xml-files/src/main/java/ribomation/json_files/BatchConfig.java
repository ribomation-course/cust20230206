package ribomation.json_files;

import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.JsonObjectMarshaller;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.batch.item.xml.builder.StaxEventItemReaderBuilder;
import org.springframework.batch.item.xml.builder.StaxEventItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import java.util.Calendar;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("xml-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("xml-to-xml-step")
                .<Person, Person>chunk(10)
                .reader(mkXmlReader())
                .processor(mkProcessor())
                .writer(mkXmlWriter())
                .build();
    }

    @Bean
    Resource xmlFile() {
        return new FileSystemResource("./build/files/persons.xml");
    }

    @Bean
    ItemProcessor<Person, Person> mkProcessor() {
        return item -> {
            item.setName(item.getName().toUpperCase());
            item.setAge(item.getAge() + 42);
            var cal = Calendar.getInstance();
            cal.setTime(item.getUpdated());
            cal.add(Calendar.DAY_OF_MONTH, 45);
            item.setUpdated(cal.getTime());
            return item;
        };
    }

    @Bean
    @SneakyThrows
    ItemReader<Person> mkXmlReader() {
        var unmarshaller = new Jaxb2Marshaller();
        unmarshaller.setClassesToBeBound(Person.class);
        unmarshaller.afterPropertiesSet();

        return new StaxEventItemReaderBuilder<Person>()
                .name("xml-reader")
                .resource(new ClassPathResource("persons.xml"))
                .addFragmentRootElements("person")
                .unmarshaller(unmarshaller)
                .build();
    }

    @Bean
    @SneakyThrows
    ItemWriter<Person> mkXmlWriter() {
        var marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(Person.class);
        marshaller.afterPropertiesSet();

        return new StaxEventItemWriterBuilder<Person>()
                .name("xml-writer")
                .resource(xmlFile())
                .marshaller(marshaller)
                .overwriteOutput(true)
                .build();
    }

}
