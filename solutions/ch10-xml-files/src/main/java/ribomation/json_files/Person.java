package ribomation.json_files;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
public class Person {
//    <person>
//        <name>Kassia Peasee</name>
//        <age>20</age>
//        <female>false</female>
//        <email>kpeasee0@google.com.br</email>
//        <updated>2022-02-18 20:18:09</updated>
//    </person>
    private String name;
    private int age;
    private boolean female;
    private String email;
    private Date updated;

    @XmlJavaTypeAdapter(DateConverter.class)
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public static class DateConverter extends XmlAdapter<String,Date> {
        final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        @Override
        public Date unmarshal(String v) throws Exception {
            return fmt.parse(v);
        }

        @Override
        public String marshal(Date v) throws Exception {
            return fmt.format(v);
        }
    }

}
