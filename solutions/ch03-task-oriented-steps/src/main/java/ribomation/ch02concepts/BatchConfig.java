package ribomation.ch02concepts;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("list-dir-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("list-dir-step")
                .tasklet((cont, ctx) -> {
                    var exe = new ProcessBuilder("ls", "-lhFA", ".").start();
                    var stdout = new BufferedReader(new InputStreamReader(exe.getInputStream()));
                    try (stdout) {
                        var linecnt = 1;
                        for (var line=stdout.readLine(); line!=null; line=stdout.readLine())
                            System.out.printf("%2d] %s%n", linecnt++, line);
                    }
                    return RepeatStatus.FINISHED;
                })
                .build();
    }
}
