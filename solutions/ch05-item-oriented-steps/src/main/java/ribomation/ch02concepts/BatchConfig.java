package ribomation.ch02concepts;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.*;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("dummy-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("dummy-step")
                .<Person, Person>chunk(3)
                .reader(mkReader())
                .processor(mkProcessor())
                .writer(mkWriter())
                .build();
    }

    @Bean
    ItemReader<Person> mkReader() {
        final List<Person> items = List.of(
                Person.builder().name("Anna").age(22).email("anna@gmail.com").build(),
                Person.builder().name("Berit").age(32).email("berit@gmail.com").build(),
                Person.builder().name("Carin").age(42).email("carin@gmail.com").build(),
                Person.builder().name("Doris").age(52).email("doris@gmail.com").build(),
                Person.builder().name("Eva").age(62).email("eva@gmail.com").build(),
                Person.builder().name("Fia").age(72).email("fia@gmail.com").build(),
                Person.builder().name("Gun").age(82).email("gun@gmail.com").build()
        );

        return new ItemReader<>() {
            int nextIndex = 0;

            @Override
            public Person read() throws Exception {
                if (nextIndex < items.size()) return items.get(nextIndex++);
                return null;
            }
        };
    }

    @Bean
    ItemProcessor<Person, Person> mkProcessor() {
        return p -> {
            p.setName(p.getName().toUpperCase());
            p.setAge(p.getAge() + 8);
            return p;
        };
    }

    @Bean
    ItemWriter<Person> mkWriter() {
        return new ItemWriter<>() {
            int chunk = 1;

            @Override
            public void write(List<? extends Person> items) throws Exception {
                int item = 1;
                for (var p : items) {
                    System.out.printf("[%d:%d] %s%n", chunk, item++, p);
                }
                ++chunk;
            }
        };
    }

}
