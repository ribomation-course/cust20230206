#!/usr/bin/env bash
set -eux

java -cp ./database/h2.jar org.h2.tools.Server -tcp -tcpPort 9100 -web -webPort 9000 -ifNotExists -baseDir ./database
