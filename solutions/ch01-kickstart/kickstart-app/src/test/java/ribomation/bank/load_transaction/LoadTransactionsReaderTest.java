package ribomation.bank.load_transaction;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStreamSupport;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import ribomation.bank.domain.Transaction;
import ribomation.bank.load_transactions.ReaderConfig;

import static org.junit.jupiter.api.Assertions.*;

public class LoadTransactionsReaderTest {

    private static ItemReader<Transaction> getReader(String txFile) throws Exception {
        var step = new ReaderConfig() {
            @Override
            public Resource transactionsFile() {
                return new ClassPathResource(txFile);
            }
        };
        var reader = step.itemReader1();
        ((ItemStreamSupport) reader).open(new ExecutionContext());
        return reader;
    }

    @Test
    @DisplayName("reading 3 tx lines, should work")
    public void test1() throws Exception {
        ItemReader<Transaction> reader = getReader("/tx1.csv");

        var tx = reader.read();
        assertNotNull(tx);
        assertEquals("0701-314996", tx.getAccno());
        assertEquals(246, tx.getAmount());

        assertNotNull(reader.read());
        assertNotNull(reader.read());
        assertNull(reader.read());
    }

    @Test
    @DisplayName("reading a tx header should throw an exception")
    public void test2() throws Exception {
        ItemReader<Transaction> reader = getReader("/tx2.csv");

        assertNotNull(reader.read());
        assertNotNull(reader.read());
        assertThrows(FlatFileParseException.class, reader::read);
        assertNotNull(reader.read());
        assertNotNull(reader.read());
        assertNull(reader.read());
    }


}
