package ribomation.bank.load_transaction;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ribomation.bank.domain.Transaction;
import ribomation.bank.load_transactions.WriterConfig;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LoadTransactionsWriterTest {
    private ItemWriter<Transaction> itemWriter;
    private JdbcTemplate jdbc;
    private EmbeddedDatabase db;

    @BeforeEach
    public void setup() {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("/tx.sql")
                .build();
        assertNotNull(db);

        itemWriter = new WriterConfig(db).itemWriter1();
        jdbc = new JdbcTemplate(db);
    }

    @AfterEach
    public void shutdown() {
        db.shutdown();
    }

    private Integer rowCount() {
        return jdbc.queryForObject("select count(*) from transactions", Integer.class);
    }

    @Test
    @DisplayName("Loading one tx objects, should work")
    public void test1() throws Exception {
        var tx = new Transaction(new Date(), "1234-567890", 100, "what ever");
        itemWriter.write(List.of(tx));

        assertEquals(1, rowCount());
    }

    @Test
    @DisplayName("Loading a bunch of tx objects, should work too")
    public void test2() throws Exception {
        var txLst = List.of(
                new Transaction(new Date(), "1234-567890", 100, "what ever"),
                new Transaction(new Date(), "1111-111111", 200, "qwerty"),
                new Transaction(new Date(), "2222-222222", 300, "asdfgh"),
                new Transaction(new Date(), "3333-333333", 400, "zxcvbn")
        );

        itemWriter.write(txLst);
        assertEquals(4, rowCount());
    }


}
