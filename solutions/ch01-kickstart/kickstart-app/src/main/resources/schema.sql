drop table if exists transactions;
drop table if exists accounts;

create table accounts (
  accno CHAR(11) primary key,
  balance INT,
  customer_name VARCHAR(50),
  email VARCHAR(50),
  street VARCHAR(50)
);

create table transactions (
  id INT primary key auto_increment,
  date DATE,
  accno CHAR(11),
  amount INT,
  what VARCHAR(50),
  foreign key(accno) references accounts(accno)
);
