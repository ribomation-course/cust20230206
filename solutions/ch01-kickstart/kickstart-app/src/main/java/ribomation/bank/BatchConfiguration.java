package ribomation.bank;


import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableBatchProcessing
@Configuration
public class BatchConfiguration {
    @Autowired JobRepository jobRepo;
    @Autowired Step loadTransactionsStep;
    @Autowired Step updateAccounts;

    @Bean
    public JobParametersIncrementer jobIdIncrementer() {
        return new RunIdIncrementer();
    }

    @Bean
    public Job bankJob() {
        return new JobBuilder("bank")
                .repository(jobRepo)
                .incrementer(jobIdIncrementer())
                .start(loadTransactionsStep)
                .next(updateAccounts)
                .build();
    }
}
