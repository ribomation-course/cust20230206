package ribomation.bank.update_accounts;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ribomation.bank.domain.Account;
import ribomation.bank.domain.Transaction;

@Configuration
public class ProcessorConfig2 {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Bean
    public RowMapper<Account> accountMapper() {
        return (rs, rowNum) -> {
            var a = new Account();
            a.setAccno(rs.getString("accno"));
            a.setBalance(rs.getInt("balance"));
            return a;
        };
    }

    @Bean
    public ItemProcessor<Transaction, Account> itemProcessor2() {
        return tx -> {
            var sql = "select accno,balance from accounts where accno = :accno";
            var acc = jdbcTemplate.queryForObject(sql, new BeanPropertySqlParameterSource(tx), accountMapper());
            if (acc == null) {
                throw new RuntimeException("Account accno = '" + tx.getAccno() + "' not found");
            }
            acc.setBalance(acc.getBalance() + tx.getAmount());
            return acc;
        };
    }

}
