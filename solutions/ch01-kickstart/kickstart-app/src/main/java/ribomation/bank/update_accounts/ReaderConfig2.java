package ribomation.bank.update_accounts;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;
import ribomation.bank.domain.Transaction;

import javax.sql.DataSource;

@Configuration
public class ReaderConfig2 {
    @Autowired
    DataSource dataSource;

    @Bean
    public RowMapper<Transaction> transactionMapper() {
        return (rs, rowNum) -> {
            var t = new Transaction();
            t.setAccno(rs.getString("accno"));
            t.setAmount(rs.getInt("amount"));
            return t;
        };
    }

    @Bean
    public ItemReader<Transaction> itemReader2() throws Exception {
        return new JdbcCursorItemReaderBuilder<Transaction>()
                .name("update-accounts-item-reader")
                .dataSource(dataSource)
                .rowMapper(transactionMapper())
                .sql("select accno,amount from transactions")
                .build();
    }
}
