package ribomation.bank.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String accno;
    private int balance;
    private String customer_name;
    private String email;
    private String street;
}
