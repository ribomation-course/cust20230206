package ribomation.bank.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    private Date date;
    private String accno;
    private int amount;
    private String what;
    public static final String[] names = {"date", "accno", "amount", "what"};
}
