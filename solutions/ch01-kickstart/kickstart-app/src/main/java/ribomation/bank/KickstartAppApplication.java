package ribomation.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KickstartAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(KickstartAppApplication.class, args);
    }

}
