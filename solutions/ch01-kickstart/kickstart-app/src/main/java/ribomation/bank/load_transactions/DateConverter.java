package ribomation.bank.load_transactions;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        try {
            var fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return fmt.parse(source);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
