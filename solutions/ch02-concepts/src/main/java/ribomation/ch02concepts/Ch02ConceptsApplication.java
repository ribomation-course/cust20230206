package ribomation.ch02concepts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch02ConceptsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch02ConceptsApplication.class, args);
    }

}
