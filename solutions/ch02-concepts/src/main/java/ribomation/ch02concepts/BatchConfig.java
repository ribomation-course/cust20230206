package ribomation.ch02concepts;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    Job mkJob() {
        return jobBldr.get("my-job")
                .incrementer(mkInc())
                .start(mkStep1())
                .next(mkStep2())
                .next(mkStep3())
                .listener(new JobExecutionListener() {
                    @Override
                    public void beforeJob(JobExecution jobExecution) {
                        System.out.println("before job");
                    }
                    @Override
                    public void afterJob(JobExecution jobExecution) {
                        System.out.println("after job");
                    }
                })
                .build();
    }

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Step mkStep1() {
        return stepBldr.get("my-step-1")
                .tasklet((cont, ctx) -> {
                    System.out.println("my step #1");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    Step mkStep2() {
        return stepBldr.get("my-step-2")
                .tasklet((cont, ctx) -> {
                    System.out.println("the step #2");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    Step mkStep3() {
        return stepBldr.get("my-step-3")
                .tasklet((cont, ctx) -> {
                    System.out.println("finally, the 3rd step");
                    return RepeatStatus.FINISHED;
                })
                .listener(new StepExecutionListener() {
                    @Override
                    public void beforeStep(StepExecution stepExecution) {
                        System.out.println("before 3rd step");
                    }

                    @Override
                    public ExitStatus afterStep(StepExecution stepExecution) {
                        System.out.println("after 3rd step");
                        return ExitStatus.COMPLETED;
                    }
                })
                .build();
    }
}
