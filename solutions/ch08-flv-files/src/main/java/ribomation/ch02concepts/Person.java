package ribomation.ch02concepts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import java.util.Date;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants(asEnum = true, innerTypeName = "namesEnum")
public class Person {
    //name,age,gender,email,updated
    private String name;
    private int age;
    private String gender;
    private String email;
    private Date updated;
    public static final String[] names = Stream
            .of(namesEnum.values())
            .map(Enum::name)
            .toArray(String[]::new);
}
