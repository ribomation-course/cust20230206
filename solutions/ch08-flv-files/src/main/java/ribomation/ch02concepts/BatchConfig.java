package ribomation.ch02concepts;

import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.validation.BindException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("flv-job")
                .incrementer(mkInc())
                .start(mkStep1())
                .next(mkStep2())
                .build();
    }

    @Bean
    Step mkStep1() {
        return stepBldr.get("csv-to-flv-step")
                .<Person, Person>chunk(10)
                .reader(mkReader())
                .processor(mkProcessor())
                .writer(mkFlvWriter())
                .build();
    }

    @Bean
    Step mkStep2() {
        return stepBldr.get("flv-to-std-step")
                .<Person, Person>chunk(10)
                .reader(mkFlvReader())
                .writer(mkStdWriter())
                .build();
    }

    @Bean
    @SneakyThrows
    ItemReader<Person> mkReader() {
        var mapper = new BeanWrapperFieldSetMapper<Person>();
        mapper.setTargetType(Person.class);
        mapper.setConversionService(new DefaultConversionService() {
            {
                addConverter(new Converter<String, Date>() {
                    final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

                    @Override
                    public Date convert(String txt) {
                        try {
                            return fmt.parse(txt);
                        } catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            }
        });
        mapper.afterPropertiesSet();

        return new FlatFileItemReaderBuilder<Person>()
                .name("csv-reader")
                .resource(new ClassPathResource("persons.csv"))
                .linesToSkip(1)
                .fieldSetMapper(mapper)
                .delimited()
                .delimiter(",")
                .names(Person.names)
                .build();
    }

    @Bean
    ItemProcessor<Person, Person> mkProcessor() {
        return item -> {
            item.setName(item.getName().toUpperCase());
            item.setAge(item.getAge() + 42);
            var cal = Calendar.getInstance();
            cal.setTime(item.getUpdated());
            cal.add(Calendar.DAY_OF_MONTH, 45);
            item.setUpdated(cal.getTime());
            return item;
        };
    }

    @Bean
    Resource flvFile() {
        return new FileSystemResource("./build/files/persons.flv");
    }

    @Bean
    ItemWriter<Person> mkFlvWriter() {
        var extractor = new FieldExtractor<Person>() {
            final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

            @Override
            public Object[] extract(Person p) {
                return new Object[]{
                        p.getName(),
                        p.getAge(),
                        p.getGender().equals("Female") ? "F" : "M",
                        p.getEmail(),
                        fmt.format(p.getUpdated())
                };
            }
        };

        var lineAggregator = new FormatterLineAggregator<Person>();
        //name,age,gender,email,updated
        //20  ,3  ,1     ,35,  ,10
        lineAggregator.setFormat("%-20.20s%03d%s%-35.35s%10s");
        lineAggregator.setFieldExtractor(extractor);

        return new FlatFileItemWriterBuilder<Person>()
                .name("flv-writer")
                .resource(flvFile())
                .shouldDeleteIfExists(true)
                .lineAggregator(lineAggregator)
                .build();
    }

    @Bean
    ItemReader<Person> mkFlvReader() {
        var tokenizer = new FixedLengthTokenizer();
        tokenizer.setNames(Person.names);
        //CONNIE HEARLEY      111Fchearley0@google.ca                2022-09-28
        //1-20                21-23,24-24,25-59                      60-69
        tokenizer.setColumns(
                new Range(1, 20),
                new Range(21, 23),
                new Range(24,24),
                new Range(25, 59),
                new Range(60,69)
        );

        final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        FieldSetMapper<Person> mapper = fs -> {
            var p = new Person();
            try {
                p.setName(fs.readString("name"));
                p.setAge(fs.readInt("age"));
                var G = fs.readString("gender");
                p.setGender(G.equals("F") ? "female" : (G.equals("M") ? "male" : "*" + G + "*"));
                p.setEmail(fs.readString("email"));
                p.setUpdated(fmt.parse(fs.readString("updated")));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return p;
        };

        var lineMapper = new DefaultLineMapper<Person>();
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(mapper);
        lineMapper.afterPropertiesSet();

        return new FlatFileItemReaderBuilder<Person>()
                .name("flv-reader")
                .resource(flvFile())
                .lineMapper(lineMapper)
                .build();
    }

    @Bean
    ItemWriter<Person> mkStdWriter() {
        return new ItemWriter<Person>() {
            int chunk = 1;

            @Override
            public void write(List<? extends Person> items) throws Exception {
                int item = 1;
                for (var p : items) {
                    System.out.printf("%d:%d) %s%n", chunk, item, p);
                    ++item;
                }
                ++chunk;
            }
        };
    }


}
