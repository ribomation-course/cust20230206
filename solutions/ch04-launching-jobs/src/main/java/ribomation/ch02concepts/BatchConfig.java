package ribomation.ch02concepts;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job anna() {
        return jobBldr.get("anna")
                .incrementer(mkInc())
                .start(mkStepAnna())
                .build();
    }

    @Bean
    Step mkStepAnna() {
        return stepBldr.get("step-anna")
                .tasklet((cont, ctx) -> {
                    System.out.println("Hello from Anna, the cute step");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    Job berra() {
        return jobBldr.get("berra")
                .incrementer(mkInc())
                .start(mkStepBerra())
                .build();
    }

    @Bean
    Step mkStepBerra() {
        return stepBldr.get("step-berra")
                .tasklet((cont, ctx) -> {
                    System.out.println("Hello from Berra, the cool step");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }
}
