package ribomation.ch02concepts;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.ApplicationContext;

@RestController
@RequestMapping("/batch")
public class LaunchController {
    @Autowired private ApplicationContext ctx;
    @Autowired private JobLauncher jobs;
    @Autowired private JobExplorer jobExplorer;

    @GetMapping("/start/{name}")
    public ExitStatus run_job(@PathVariable(required = false) String name) throws Exception {
        if (name == null) name = "anna";
        System.out.println("[rest-ws] starting job named " + name);
        var job = ctx.getBean(name, Job.class);
        var params = new JobParametersBuilder(jobExplorer).getNextJobParameters(job).toJobParameters();
        var exe = jobs.run(job, params);
        return exe.getExitStatus();
    }
}
