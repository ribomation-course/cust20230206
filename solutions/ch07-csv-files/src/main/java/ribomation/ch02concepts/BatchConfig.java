package ribomation.ch02concepts;

import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("dummy-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("dummy-step")
                .<Person, Person>chunk(10)
                .reader(mkReader())
                .processor(mkProcessor())
                .writer(mkWriter())
                .build();
    }

    @Bean
    @SneakyThrows
    ItemReader<Person> mkReader() {
        var mapper = new BeanWrapperFieldSetMapper<Person>();
        mapper.setTargetType(Person.class);
        mapper.setConversionService(new DefaultConversionService() {
            {
                addConverter(new Converter<String, Date>() {
                    final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

                    @Override
                    public Date convert(String txt) {
                        try {
                            return fmt.parse(txt);
                        } catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            }
        });
        mapper.afterPropertiesSet();

        return new FlatFileItemReaderBuilder<Person>()
                .name("csv-reader")
                .resource(new ClassPathResource("persons.csv"))
                .linesToSkip(1)
                .fieldSetMapper(mapper)
                .delimited()
                .delimiter(",")
                .names(Person.names)
                .build();
    }

    @Bean
    ItemProcessor<Person, Person> mkProcessor() {
        return item -> {
            item.setName(item.getName().toUpperCase());
            item.setAge(item.getAge() + 42);
            var cal = Calendar.getInstance();
            cal.setTime(item.getUpdated());
            cal.add(Calendar.DAY_OF_MONTH, 45);
            item.setUpdated(cal.getTime());
            return item;
        };
    }

    @Bean
    ItemWriter<Person> mkWriter() {
        return new FlatFileItemWriterBuilder<Person>()
                .name("csv-writer")
                .resource(new FileSystemResource("./build/tmp/persons.csv"))
                .shouldDeleteIfExists(true)
                .delimited()
                .delimiter(";")
                .names(Person.names)
                .build();
    }

}
