package ribomation.json_files;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.JsonObjectMarshaller;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.util.Calendar;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("json-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("json-to-json-step")
                .<Person, Person>chunk(10)
                .reader(mkJsonReader())
                .processor(mkProcessor())
                .writer(mkJsonWriter())
                .build();
    }

    @Bean
    Resource jsonFile() {
        return new FileSystemResource("./build/files/persons.json");
    }

    @Bean
    ItemProcessor<Person, Person> mkProcessor() {
        return item -> {
            item.setName(item.getName().toUpperCase());
            item.setAge(item.getAge() + 42);
            var cal = Calendar.getInstance();
            cal.setTime(item.getUpdated());
            cal.add(Calendar.DAY_OF_MONTH, 45);
            item.setUpdated(cal.getTime());
            return item;
        };
    }

    @Bean
    ItemReader<Person> mkJsonReader() {
        var jsonReader = new JacksonJsonObjectReader<>(Person.class);

        return new JsonItemReaderBuilder<Person>()
                .name("json-reader")
                .resource(new ClassPathResource("persons.json"))
                .jsonObjectReader(jsonReader)
                .build();
    }

    @Bean
    ItemWriter<Person> mkJsonWriter() {
        JsonObjectMarshaller<Person> jsonMarshaller = new JacksonJsonObjectMarshaller<>();

        return new JsonFileItemWriterBuilder<Person>()
                .name("json-writer")
                .resource(jsonFile())
                .jsonObjectMarshaller(jsonMarshaller)
                .shouldDeleteIfExists(true)
                .build();
    }

}
