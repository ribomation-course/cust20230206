package ribomation.json_files;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Person {
    //{"name":"Shaylyn Blackler","age":51,"gender":"Female","email":"sblackler1@apache.org","updated":"2022-12-08 14:44:48"},

    @JsonProperty private String name;
    @JsonProperty private int age;
    @JsonProperty private String gender;
    @JsonProperty private String email;

    @JsonProperty
    @JsonDeserialize(using = DateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date updated;

    public static class DateDeserializer extends JsonDeserializer<Date> {
        final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        @Override
        @SneakyThrows
        public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
            return fmt.parse(p.getText());
        }
    }

    public static class DateSerializer extends JsonSerializer<Date> {
        final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        @Override
        public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(fmt.format(value));
        }
    }

}
