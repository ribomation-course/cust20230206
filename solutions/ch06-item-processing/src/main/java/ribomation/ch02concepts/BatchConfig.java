package ribomation.ch02concepts;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.*;
import org.springframework.batch.item.support.builder.CompositeItemProcessorBuilder;
import org.springframework.batch.item.validator.BeanValidatingItemProcessor;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("dummy-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("dummy-step")
                .<Person, Person>chunk(3)
                .reader(mkReader())
                .processor(mkProcessor())
                .writer(mkWriter())
                .build();
    }

    @Bean
    ItemProcessor<Person, Person> mkProcessor() {
        return new CompositeItemProcessorBuilder<Person,Person>()
                .delegates(
                        mkAutoValidation(),
                        mkDropEachThird()
                        )
                .build();
    }

    @Bean
    ItemProcessor<Person, Person> mkAutoValidation() {
        var proc = new BeanValidatingItemProcessor<Person>();
        proc.setFilter(true);
        return proc;
    }

    @Bean
    ItemProcessor<Person, Person> mkDropEachThird() {
        return new ItemProcessor<>() {
            int count = 0;

            @Override
            public Person process(Person item) throws Exception {
                if (++count % 3 == 0) return null;
                return item;
            }
        };
    }

    @Bean
    ItemReader<Person> mkReader() {
        final List<Person> items = List.of(
                Person.builder().name("Anna").age(22).email("anna@gmail.com").build(),
                Person.builder().name("Berit").age(32).email("berit@gmail.com").build(),
                Person.builder().name("Carin").age(42).email("carin@gmail.com").build(),    //dropped
                Person.builder().name("Doris").age(52).email("doris@gmail.com").build(),
                Person.builder().name("Eva").age(62).email("eva@gmail.com").build(),
                Person.builder().name("Fia").age(22).email("fia@gmail.com").build(),        //dropped
                Person.builder().name("Gun").age(32).email("gun@gmail.com").build(),
                Person.builder().name("Hanna").age(42).email("hanna@gmail@com").build(),    //faulty
                Person.builder().name("Ida").age(2).email("ida@gmail.com").build(),         //faulty
                Person.builder().name("").age(62).email("jonna@gmail.com").build()          //faulty
        );

        return new ItemReader<>() {
            int nextIndex = 0;

            @Override
            public Person read() throws Exception {
                if (nextIndex < items.size()) return items.get(nextIndex++);
                return null;
            }
        };
    }

    @Bean
    ItemWriter<Person> mkWriter() {
        return new ItemWriter<>() {
            int chunk = 1;

            @Override
            public void write(List<? extends Person> items) throws Exception {
                int item = 1;
                for (var p : items) {
                    System.out.printf("[%d:%d] %s%n", chunk, item++, p);
                }
                ++chunk;
            }
        };
    }

}
