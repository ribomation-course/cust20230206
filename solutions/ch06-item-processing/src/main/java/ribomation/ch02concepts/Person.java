package ribomation.ch02concepts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@Builder
public class Person {
    @NotEmpty
    private String name;
    @Min(20) @Max(80)
    private int age;
    @Email
    private String email;
}
