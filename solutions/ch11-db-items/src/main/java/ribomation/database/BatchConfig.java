package ribomation.database;

import lombok.SneakyThrows;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired JobBuilderFactory jobBldr;
    @Autowired StepBuilderFactory stepBldr;
    @Autowired DataSource ds;

    @Bean
    RunIdIncrementer mkInc() {
        return new RunIdIncrementer();
    }

    @Bean
    Job mkJob() {
        return jobBldr.get("db-job")
                .incrementer(mkInc())
                .start(mkStep())
                .build();
    }

    @Bean
    Step mkStep() {
        return stepBldr.get("dummy-step")
                .<Account, Account>chunk(100)
                .reader(mkReader())
                .processor(mkProcessor())
                .writer(mkWriter())
                .listener(new StepExecutionListener() {
                    @Override
                    public void beforeStep(StepExecution stepExecution) {
                        computeBalance("initial");
                    }

                    @Override
                    public ExitStatus afterStep(StepExecution stepExecution) {
                        computeBalance("final");
                        return stepExecution.getExitStatus();
                    }

                    private void computeBalance(String prefix) {
                        var jdbc = new JdbcTemplate(ds);
                        var totBal = jdbc.queryForObject("SELECT sum(a.balance) FROM accounts a", Integer.class);
                        System.out.printf("%s balance = %d%n", prefix, totBal);
                    }
                })
                .build();
    }

    @Bean
    @SneakyThrows
    ItemReader<Account> mkReader() {
        var sql = "select * from accounts";
        return new JdbcCursorItemReaderBuilder<Account>()
                .name("db-reader")
                .dataSource(ds)
                .sql(sql)
                .beanRowMapper(Account.class)
                .build();
    }

    @Bean
    ItemProcessor<Account, Account> mkProcessor() {
        return item -> {
            //balance *= (1 + rate / 100)
            item.setBalance((int) (item.getBalance() * (1 + item.getRate() / 100.0)));
            return item;
        };
    }

    @Bean
    ItemWriter<Account> mkWriter() {
        var sql = "UPDATE accounts SET balance = :balance WHERE accno=:accno";
        return new JdbcBatchItemWriterBuilder<Account>()
                .dataSource(ds)
                .sql(sql)
                .beanMapped()
                .build();
    }
}
