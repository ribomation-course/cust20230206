package ribomation.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.util.Date;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants(asEnum = true, innerTypeName = "fieldsEnum")
public class Account {
//    accno    VARCHAR(50) primary key,
//    balance  INT,
//    rate     DECIMAL(3, 2),
//    updated  DATE,
//    customer VARCHAR(50),
//    city     VARCHAR(50)

    private String accno;
    private Integer balance;
    private Float rate;
    private Date updated;
    private String customer;
    private String city;
    public static final String[] names = Stream
            .of(fieldsEnum.values())
            .map(Enum::name)
            .toArray(String[]::new);
}
