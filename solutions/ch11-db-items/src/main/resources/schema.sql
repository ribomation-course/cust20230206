create table accounts
(
    accno    VARCHAR(50) primary key,
    balance  INT,
    rate     DECIMAL(3, 2),
    updated  DATETIME,
    customer VARCHAR(50),
    city     VARCHAR(50)
);
