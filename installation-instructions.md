# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the same setup as for the _Spring_ course.

* SDKMAN
* Java JDK
* Gradle
* Intellij IDEA

