package ribomation.kickstart_app.update_accounts;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ribomation.kickstart_app.domain.Account;

import javax.sql.DataSource;

@Configuration
public class WriterConfig2 {
    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    @Bean
    public ItemWriter<Account> itemWriter2() {
        var sql = "update accounts set balance = :balance where accno = :accno";
//        return new JdbcBatchItemWriterBuilder<Account>()
//                .beanMapped()
//                .namedParametersJdbcTemplate(jdbcTemplate)
//                .sql(sql)
//                .build();

        var w = new JdbcBatchItemWriter<Account>();
        w.setSql(sql);
        w.setJdbcTemplate(jdbcTemplate);
        w.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        w.afterPropertiesSet();
        return w;
    }

}
