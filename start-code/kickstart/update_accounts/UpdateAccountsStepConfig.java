package ribomation.kickstart_app.update_accounts;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import ribomation.kickstart_app.domain.Account;
import ribomation.kickstart_app.domain.Transaction;

@Configuration
public class UpdateAccountsStepConfig {
    @Value("${app.chunk.size}")
    int chunkSize = 1;

    @Autowired
    JobRepository jobRepo;

    @Autowired
    PlatformTransactionManager txManager;

    @Autowired
    JdbcTemplate jdbc;

    @Autowired
    ItemReader<Transaction> itemReader2;

    @Autowired
    ItemProcessor<Transaction, Account> itemProcessor2;

    @Autowired
    ItemWriter<Account> itemWriter2;

    @Bean
    public Step updateAccounts() throws Exception {
        return new StepBuilder("update-accounts")
                .repository(jobRepo)
                .transactionManager(txManager)
                .<Transaction, Account>chunk(chunkSize)
                .reader(itemReader2)
                .processor(itemProcessor2)
                .writer(itemWriter2)
                .listener(new StepSummary(jdbc))
                .build();
    }
}
