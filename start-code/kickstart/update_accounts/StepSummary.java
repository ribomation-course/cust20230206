package ribomation.kickstart_app.update_accounts;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.jdbc.core.JdbcTemplate;

public class StepSummary extends StepExecutionListenerSupport {
    private final JdbcTemplate jdbc;

    public StepSummary(JdbcTemplate jdbcTemplate) {
        this.jdbc = jdbcTemplate;
    }

    @Override
    public ExitStatus afterStep(StepExecution ctx) {
        var totBalance = jdbc.queryForObject("select sum(a.balance) from accounts a", Long.class);

        System.out.printf("[%s] %s%nTotal ACC Balance = %d kr%n",
                ctx.getStepName(), ctx.getSummary(), totBalance);

        return ctx.getExitStatus();
    }

}
