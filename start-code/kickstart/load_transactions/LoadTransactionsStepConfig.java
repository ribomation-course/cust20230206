package ribomation.kickstart_app.load_transactions;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import ribomation.kickstart_app.domain.Transaction;

@Configuration
public class LoadTransactionsStepConfig {
    @Value("${app.chunk.size}")
    int chunkSize = 1;

    @Value("${app.chunk.skipMax}")
    int skipMax = 1;

    @Autowired
    JdbcTemplate jdbc;

    @Autowired
    JobRepository jobRepo;

    @Autowired
    PlatformTransactionManager txManager;

    @Autowired
    ItemReader<Transaction> itemReader1;

    @Autowired
    ItemWriter<Transaction> itemWriter1;

    @Bean
    public Step loadTransactionsStep() throws Exception {
        return new StepBuilder("load-transactions")
                .repository(jobRepo)
                .transactionManager(txManager)
                .<Transaction, Transaction>chunk(chunkSize)
                .reader(itemReader1)
                .writer(itemWriter1)
                .faultTolerant()
                .skipLimit(skipMax)
                .skip(FlatFileParseException.class)
                .listener(new StepSummary(jdbc))
                .build();
    }
}
