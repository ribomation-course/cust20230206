package ribomation.kickstart_app.load_transactions;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ribomation.kickstart_app.domain.Transaction;

import javax.sql.DataSource;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.of;
import static ribomation.kickstart_app.domain.Transaction.names;

@Configuration
public class WriterConfig {
    private final DataSource dataSource;

    public WriterConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public ItemWriter<Transaction> itemWriter1() {
        var sql = format("insert into transactions (%s) values (%s)",
                of(names).collect(joining(",")),
                of(names).map(n -> ":" + n).collect(joining(","))
        );
        var w = new JdbcBatchItemWriter<Transaction>();
        w.setSql(sql);
        w.setDataSource(dataSource);
        w.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        w.afterPropertiesSet();
        return w;
    }

}
