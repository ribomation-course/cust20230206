package ribomation.kickstart_app.load_transactions;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import ribomation.kickstart_app.domain.Transaction;

import static ribomation.kickstart_app.domain.Transaction.names;

@Configuration
public class ReaderConfig {

    @Value("${app.file.transactions}")
    String transactionsFilename;

    @Bean
    public Resource transactionsFile() {
        return new FileSystemResource(transactionsFilename);
    }

    @Bean
    public ItemReader<Transaction> itemReader1() throws Exception {
        var dateConversionSvc = new DefaultConversionService();
        dateConversionSvc.addConverter(new DateConverter());

        var tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter(";");
        tokenizer.setNames(names);

        var beanMapper = new BeanWrapperFieldSetMapper<Transaction>();
        beanMapper.setTargetType(Transaction.class);
        beanMapper.setConversionService(dateConversionSvc);
        beanMapper.afterPropertiesSet();

        var lineMapper = new DefaultLineMapper<Transaction>();
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(beanMapper);
        lineMapper.afterPropertiesSet();

        var reader = new FlatFileItemReader<Transaction>();
        reader.setResource(transactionsFile());
        reader.setLineMapper(lineMapper);
        reader.setName("load-transactions-item-reader");
        reader.afterPropertiesSet();

        return reader;
    }

}
