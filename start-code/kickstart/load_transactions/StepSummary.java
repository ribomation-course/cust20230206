package ribomation.kickstart_app.load_transactions;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.jdbc.core.JdbcTemplate;


public class StepSummary extends StepExecutionListenerSupport {
    private final JdbcTemplate jdbc;

    public StepSummary(JdbcTemplate jdbcTemplate) {
        this.jdbc = jdbcTemplate;
    }

    @Override
    public ExitStatus afterStep(StepExecution ctx) {
        var totBalance = jdbc.queryForObject("select sum(a.balance) from accounts a", Long.class);
        var totAmount = jdbc.queryForObject("select sum(t.amount) from transactions t", Long.class);

        System.out.printf("[%s] %s%n"
                          + "Total TX  Amount  = %d kr%n"
                          + "Total ACC Balance = %d kr%n",
                ctx.getStepName(), ctx.getSummary(), totAmount, totBalance);

        return ctx.getExitStatus();
    }

}
