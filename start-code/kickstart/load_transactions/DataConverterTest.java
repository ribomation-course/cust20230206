package ribomation.kickstart_app.load_transactions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataConverterTest {

    @Test
    @DisplayName("parsing a well-formed date, should work")
    public void test1() {
        var text = "2022-10-24 14:25:37";
        var conv = new DateConverter();
        var date = conv.convert(text);
        var cal = new Calendar.Builder().setInstant(date).build();
        assertEquals(2022, cal.get(Calendar.YEAR));
        assertEquals(10 - 1, cal.get(Calendar.MONTH));
        assertEquals(24, cal.get(Calendar.DAY_OF_MONTH));
        assertEquals(14, cal.get(Calendar.HOUR_OF_DAY));
        assertEquals(25, cal.get(Calendar.MINUTE));
        assertEquals(37, cal.get(Calendar.SECOND));
    }

}
