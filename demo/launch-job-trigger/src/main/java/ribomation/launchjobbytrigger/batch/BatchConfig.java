package ribomation.launchjobbytrigger.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
    @Autowired private JobRepository jobRepo;
    @Autowired private PlatformTransactionManager txMgr;

    @Bean
    public Job simple_job() {
        return new JobBuilder("simple")
                .repository(jobRepo)
                .incrementer(new RunIdIncrementer())
                .start(simple_step(null))
                .build();
    }

    @Bean
    public Step simple_step(SimpleTask simpleTask) {
        return new StepBuilder("simple-step")
                .repository(jobRepo)
                .transactionManager(txMgr)
                .tasklet(simpleTask)
                .build();
    }
}

