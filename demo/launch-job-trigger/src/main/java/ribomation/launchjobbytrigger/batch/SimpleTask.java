package ribomation.launchjobbytrigger.batch;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class SimpleTask implements Tasklet {
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        System.out.println("[simple task] START");
        var file = contribution.getStepExecution().getJobParameters().getString("file");
        if (file != null && Files.exists(Path.of(file))) {
            print_file(file);
        } else {
            delay(8);
        }
        System.out.println("[simple task] DONE");
        return RepeatStatus.FINISHED;
    }

    private void delay(int N) {
        try {
            do {
                System.out.print(".");
                System.out.flush();
                Thread.sleep(500);
            } while (N-- > 0);
        } catch (InterruptedException ignore) {}
        System.out.println();
    }

    private void print_file(String filename) throws IOException {
        var p = Path.of(filename);
        var size = Files.size(p);
        System.out.printf("[simple task] file=%s, size=%d bytes%n", p, size);
        Files.move(p, p.getParent().resolve(p.getFileName().toString() + ".done"));
    }
}


