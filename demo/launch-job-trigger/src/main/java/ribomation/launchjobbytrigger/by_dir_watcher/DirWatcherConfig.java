package ribomation.launchjobbytrigger.by_dir_watcher;

import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.job.SimpleJob;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.*;
import java.util.Date;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

@Component
public class DirWatcherConfig implements Runnable {
    private final Path dir;
    private final String ext;
    private final WatchService watchSvc;
    private Thread runner;

    @Autowired private ApplicationContext ctx;
    @Autowired private JobLauncher jobs;
    @Autowired private JobExplorer jobExplorer;

    public DirWatcherConfig(
            @Value("${app.watcher.dir}") Path dir,
            @Value("${app.watcher.ext}") String ext
    ) throws IOException
    {
        this.dir = dir;
        this.ext = ext;
        watchSvc = FileSystems.getDefault().newWatchService();
        if (!Files.isDirectory(dir)) {
            throw new IllegalArgumentException("not a dir: " + dir);
        }
        dir.register(watchSvc, ENTRY_CREATE);
    }

    @PostConstruct
    public void launch() {
        runner = new Thread(this);
        runner.start();
    }

    @PreDestroy
    public void shutdown() {
        runner.interrupt();
    }

    @Override
    public void run() {
        System.out.printf("%tT [dir watcher] started: dir=%s%n", new Date(), dir);
        try {
            while (true) {
                for (WatchKey key = watchSvc.take(); key != null; key = watchSvc.take()) {
                    key.pollEvents().stream()
                            .filter(e -> e.kind().name().equals("ENTRY_CREATE"))
                            .map(e -> e.context().toString())
                            .filter(name -> name.endsWith(ext))
                            .map(dir::resolve)
                            .forEach(this::launch_job);
                    key.reset();
                }
            }
        } catch (InterruptedException ignore) {}
        System.out.printf("%tT [dir watcher] done%n", new Date());
    }

    private void launch_job(Path file) {
        System.out.printf("%tT [dir watcher] file: %s%n", new Date(), file);
        try {
            var job = ctx.getBean(SimpleJob.class);
            var params = new JobParametersBuilder(jobExplorer)
                    .addString("file", file.toString())
                    .getNextJobParameters(job)
                    .toJobParameters();
            jobs.run(job, params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
