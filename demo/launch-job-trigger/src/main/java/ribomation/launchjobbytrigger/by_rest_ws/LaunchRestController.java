package ribomation.launchjobbytrigger.by_rest_ws;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.job.SimpleJob;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/batch")
public class LaunchRestController {
    @Autowired private ApplicationContext ctx;
    @Autowired private JobLauncher jobs;
    @Autowired private JobExplorer jobExplorer;

    @GetMapping("/start")
    public ExitStatus run_job() throws Exception {
        System.out.println("[rest-ws] starting job");
        var job    = ctx.getBean(SimpleJob.class);
        var params = new JobParametersBuilder(jobExplorer).getNextJobParameters(job).toJobParameters();
        var exe    = jobs.run(job, params);
        return exe.getExitStatus();
    }
}

