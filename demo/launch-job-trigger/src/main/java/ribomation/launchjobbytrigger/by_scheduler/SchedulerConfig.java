package ribomation.launchjobbytrigger.by_scheduler;

import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.job.SimpleJob;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class SchedulerConfig {
    @Autowired private ApplicationContext ctx;
    @Autowired private JobLauncher jobs;
    @Autowired private JobExplorer jobExplorer;

    @Scheduled(initialDelay = 10 * 1000, fixedRate = 60 * 1000)
    public void invoke_simple_job() throws Exception {
        System.out.println("[scheduler] starting job");
        var job = ctx.getBean(SimpleJob.class);
        var params = new JobParametersBuilder(jobExplorer).getNextJobParameters(job).toJobParameters();
        jobs.run(job, params);
    }
}

